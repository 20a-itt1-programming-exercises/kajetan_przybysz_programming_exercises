from tkinter import *


def random(): 
	import random
	import webbrowser
	import json
	x = 0

	small = []
	num = []
	for i in range(65, 91):
		litera = chr(i)
		x += 1
		small.append(litera.lower())
	y = 0
	for j in range(48, 58):
		numers = chr(j)
		y += 1
		num.append(numers)
		
	characters = []
	nums = []
	for character in range(0,2):
		characters.append(random.choice(small))
	for num in range(0,4):
		nums.append(random.choice(str(num)))
			
	combination = characters[0] + characters[1] + nums[0]+nums[1]+nums[2]+nums[3]

	print(combination)
	used_list = []
	used_list.append(combination)
		
	if combination in used_list:
		url = 'https://prnt.sc/' + combination
		webbrowser.open(url)
		filename = 'used_sites.json'
		with open(filename, 'a') as file_object:
			json.dump(used_list, file_object)	

	if combination not in used_list:
		generate()
		
def ten_random():
	for i in range(0,10):
		random()
		
window = Tk()
window.title( "Generator random pages from prnt.sc" )	
frame = Frame(window)
frame.pack()

button1 = Button (frame, text = "Random!", command = random)
button2 = Button (frame, text = "Random! 10x", command = ten_random)
button1.pack()
button2.pack()

window.mainloop()
