def exercise_1():
	
	letters = ['a', 'b', 'c']
	
	def chop(arg):
		del arg[0]
		del arg[:-1]
		return None
		
	chop(letters)
	print(letters)
	
	def middle(arg):
		chop(letters)
		arg_2 = arg
		return arg_2
		
	print(middle(letters))
	
def exercise_2():
	
	fhand = open('mbox-short.txt')
	count = 0
	
	for line in fhand:
		words = line.split()
		# print('Debug:', words)
		
		if len(words) < 3:
			continue
			
		if words[0] != 'From' :
			continue
			
		print(words[2])

def exercise_3():
	fhand = open('mbox-short.txt')

	for line in fhand:
		words = line.split()
		# print('Debug:', words)
		
		if len(words) < 3 or words[0] != 'From':
			continue
			
		print(words[2])

def exercise_4():
	fhand = open('romeo.txt')
	list=[]
	
	for line in fhand:
		words = line.split()
		
		if len(words) == 0 : 
			continue
		
		for word in words:
			if(word not in list):
				list.append(word)
				
	list.sort()
	print(list)

def exercise_5():
	fhand = open('mbox-short.txt')
	count = 0 
	
	for line in fhand:
		words = line.split()
		
		if(len(words)<2 or words[0]!='From') :
			print(words[1])
			continue
			
	if(words[0]!='From:'): 
		count+=1
	print(f"There were {count} lines in the file with From as the first word")

def exercise_6():
	numbers = []
	 
	while True:	
		try:
			i = input('Enter number : ')
			
			if i == 'done' :
				break
			
			number = int(i)
			numbers.append(i)
		
		except:
			print("Try one more time. You can only use numbers. Enter 'done' to exit")
			
	print("Minimum : ",min(numbers)," Maximum : ",max(numbers))
	
def loop():
	exercise_1()
	exercise_2()
	exercise_3()	
	exercise_4()
	exercise_5()

if __name__ == '__main__':
	
	try:
		loop()
		
	except KeyboardInterrupt:
		import sys
		sys.exit(main(sys.argv))	
