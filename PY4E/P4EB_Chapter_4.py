def exercise_1():
	import random
	
	for i in range(0,5):
		print(str(random.randint(5, 10)))

def exercise_2():
	print("""	
		repeat_lyrics()
		
		def print_lyrics():
			print("I'm a lumberjack, and I'm okay.")
			print('I sleep all night and I work all day.')
			
		def repeat_lyrics():
			print_lyrics()
			print_lyrics()
		#Error message is:
		#UnboundLocalError: local variable 'repeat_lyrics'
		#referenced before assignment
		"""	)
def exercise_3(): 
		
	def repeat_lyrics():
		print_lyrics()
		print_lyrics
		
	def print_lyrics():
		print("I'm a lumberjack, and I'm okay.")
		print('I sleep all night and I work all day.')
		
	repeat_lyrics()
#The program is working.	

def exercise_4():
	print('The answer is B')

def exercise_5():
	print('The answer is D')
	
def exercise_6():
	
	try:
		hours = float(input("Enter hours : "))
		rate = float(input("Enter rate : "))
	
	except Exception:
		print("Error, please enter numeric input!")
		
	def computepay(hours, rate):
		if hours > 40:
			pay = (hours - 40)*rate*1.5+ 40 * rate
		else:
			pay = hours * rate
		return pay
   
	rate = int(input("Enter hours: "))
	hours = int(input("Enter rate: "))

	pay = computepay(hours, rate)

	print("Pay is: ", pay)
	
def exercise_7():

	score = float(input("Enter score : "))
	def Score():
		try:
			score = float(input("""Enter your score which is a value 
									between 0.0 - 1.0 : """))
			if score > 0.0 and score < 1.0:
				computeGrade
				(score)
			else:
				print(str(score) + ' is not a valid score!')
				Score()
		except ValueError:
			print(''' + score + ''' + ' is not a valid float!')
			Score()

	def computeGrade(score):
		print('\n')
		if score >= 0.9:
			print('Your grade is: A')
		elif score >= 0.8:
			print('Your grade is: B')
		elif score >= 0.7:
			print('Your grade is: C')
		elif score >= 0.6:
			print('Your grade is: D')
		elif score < 0.6:
			print('Your grade is: F')
		print('\n')

	Score()

def loop():
	exercise_1()
	exercise_2()
	exercise_3()	
	exercise_4()
	exercise_5()
	exercise_6()
	exercise_7()


if __name__ == '__main__':
	
	try:
		loop()
	except KeyboardInterrupt:
		import sys
		sys.exit(main(sys.argv))

	
