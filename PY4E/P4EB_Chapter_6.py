def exercise_1():
	
	fruit = 'banana'
	index = 0 
	
	while index  < len(fruit):
		letter = fruit[index]
		print(letter)
		index = index + 1
		

def exercise_2():
	print('It means the whole string from start to the end')

def exercise_3():
	
	word = 'banana'
	
	def count(character, word):
		
		count = 0
		
		for letter in word:
			
			if(letter == character):
				count = count + 1
				
		return count
		
	count=count('a', word)
	print(count)

def exercise_4():
	word = 'banana'
	count = word.count("a")
	
	print(count)

def exercise_5():
	str = 'X-DSPAM-Confidence:0.8475'
	
	portion = str.find(":")
	colon = str[portion  + 1 : len(str)]
	convert = float(colon)
	
	print(convert)

def loop():
	exercise_1()
	exercise_2()
	exercise_3()	
	exercise_4()
	exercise_5()


if __name__ == '__main__':
	
	try:
		loop()
	except KeyboardInterrupt:
		import sys
		sys.exit(main(sys.argv))
