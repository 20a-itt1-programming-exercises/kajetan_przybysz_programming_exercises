def exercise_1():

	hours = float(input("Enter hours: "))
	rate = float(input("Enter rate: "))

	if hours > 40:
		sum_1 = ((hours - 40) * rate * 1.5) + rate * 40
		print("Pay: " + str(sum1))

	elif 0 < hours <= 40:
		sum = hours * rate
		print("Pay: " + str(sum))

	elif hours <= 0 or rate <= 0:
		print("Wrong data")
		return
		
def exercise_2():
		
		try:
			hours = float(input("Enter hours : "))
			rate = float(input("Enter rate : "))
	
		except Exception:
			print("Error, please enter numeric input!")


		if hours > 40:
			sum_1 = ((hours - 40) * rate * 1.5) + rate * 40
			print("Pay : " + str(sum1))

		elif 0 < hours <= 40:
			sum = hours * rate
			print("Pay : " + str(sum))

		elif hours <= 0 or rate <= 0:
			print("Wrong data!")

def exercise_3():
	
		score = float(input("Enter score : "))
		
		if 0 >= score >= 1.0:
			
			if score >= 0.9:
				print("A")
				
			if score  >= 0.8:
				print("B")
				
			if score >= 0.7:
				print("C")
				
			if score  >= 0.6:
				print("D")
				
			if score < 0.6:
				print("F")
		else:
			print("Bad score")

def loop():
	exercise_1()
	exercise_2()
	exercise_3()

if __name__ == '__main__':
	
	try:
		loop()
	except KeyboardInterrupt:
		import sys
		sys.exit(main(sys.argv))

