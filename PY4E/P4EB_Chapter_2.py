Exercise 1:
	5
	x = 5
	x + 1 
	
Exercise 2:	
	
	def exercise_2():
		
		name = input("Write your name : ")
		print("Hello " + name + "!")
		
	exercise_2()
	
Exercise 3:
	
	def exercise_3():
		hours = input("Enter Hours : ")
		rate = input("Enter Rate : ")
	
		pay = int(hours) * int(rate)
	
		print("Pay : " + str(pay))
	
	exercise_3()

Exercise 4:
	
	def exercise_4():
		
		ex1 = 17//2
		ex2 = 17/2.0
		ex3 = 12.0/3
		ex4 = 1 + 2 * 5

		print("Result 1 : " + str(ex1))
		print("Result 2 : " + str(ex2))
		print("Result 3 : " + str(ex3))
		print("Result 4 : " + str(ex4))
		
	exercise_4()

Exercise 5:
	
	def exercise_5():
	
		celsius = input("Enter Celsius temperature : ")
		celsius = float(celsius)
		
		fahrenheit = celsius * 9/5 + 32
		
		print("Fahrenheit temperature : " + str(fahrenheit))
		
	exercise_5()
