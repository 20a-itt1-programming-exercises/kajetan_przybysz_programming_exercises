
def exercise_1():
	
	numbers = []
	
	while True:
		try:
			number = input('Enter number : ')
		
			if (number == 'done'):
				count = len(numbers)
				summary = sum(numbers)
				print('Total : ', str(count))
				print('Count : ', str(summary)) 
				try:	
					average = sum(numbers) / len(numbers)	
					print('Average : ', str(average))
					
				except ZeroDivisionError:
					 print('Try enter number.')
					 
				break	
			else:
				numbers.append(int(number))
				print(numbers)
		except ValueError :
			print("\nPlease try one more time.\nYou can use only numbers.\nTo exit enter 'done'.")

def exercise_2():

	numbers =[]

	while True:
		try:
			number =input("Enter a number: ")
			
			if(number =="done"):
				break
				
			numbers.append(int(number))
			
		except ValueError:
			print("Wrong data. Try one more time.")

	maximum = max(numbers)
	minimum = min(numbers)
	
	print('Maximum : '+str(maximum) + '\nMinimum : '+str(minimum))


def loop():
	exercise_1()
	exercise_2()



if __name__ == '__main__':
	
	try:
		loop()
		
	except KeyboardInterrupt:
		import sys
		sys.exit(main(sys.argv))
